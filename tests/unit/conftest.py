import sys
from unittest import mock

import pop
import pytest


@pytest.fixture(scope="function", name="hub")
def unit_hub(code_dir):
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        for dyne in ["idem"]:
            hub.pop.sub.add(dyne_name=dyne)

        yield hub
