import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_k8s_cluster(
    hub, ctx, aws_ec2_security_group, aws_ctx, aws_ec2_subnet, aws_image
):
    k8s_cluster_temp_name = "idem-test-k8s-cluster-" + str(uuid.uuid4())
    region = aws_ctx.acct.get("region_name")
    subnet_ids = [aws_ec2_subnet.get("SubnetId")]
    security_group_id = aws_ec2_security_group.get("resource_id")
    compute = {
        "subnetIds": subnet_ids,
        "launchSpecification": {
            "imageId": aws_image,
            "securityGroupIds": [security_group_id],
        },
    }
    capacity = {"minimum": 0, "maximum": 1000, "target": 1}
    controller_cluster_id = "ocean.k8s-11" + str(uuid.uuid4())

    # create k8s_cluster with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.spotinst.ocean.aws.k8s_cluster.present(
        test_ctx,
        name=k8s_cluster_temp_name,
        controller_cluster_id=controller_cluster_id,
        region=region,
        compute=compute,
        capacity=capacity,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create spotinst.ocean.aws.k8s_cluster '{k8s_cluster_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_k8s_cluster_states(
        resource,
        compute,
        controller_cluster_id,
        k8s_cluster_temp_name,
        region,
        capacity,
    )

    # create k8s_cluster in real
    ret = await hub.states.spotinst.ocean.aws.k8s_cluster.present(
        ctx,
        name=k8s_cluster_temp_name,
        controller_cluster_id=controller_cluster_id,
        region=region,
        compute=compute,
        capacity=capacity,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created spotinst.ocean.aws.k8s_cluster '{k8s_cluster_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_k8s_cluster_states(
        resource,
        compute,
        controller_cluster_id,
        k8s_cluster_temp_name,
        region,
        capacity,
    )

    resource_id = resource.get("resource_id")

    # Describe k8s_cluster
    describe_ret = await hub.states.spotinst.ocean.aws.k8s_cluster.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "spotinst.ocean.aws.k8s_cluster.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "spotinst.ocean.aws.k8s_cluster.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("resource_id")
    assert_k8s_cluster_states(
        described_resource_map,
        compute,
        controller_cluster_id,
        k8s_cluster_temp_name,
        region,
        capacity,
    )

    # update k8s_cluster with test flag
    updated_compute = {
        "subnetIds": subnet_ids,
        "launchSpecification": {
            "imageId": aws_image,
            "securityGroupIds": [security_group_id],
            "tags": [{"tagKey": "tag-key-1", "tagValue": "tag-value-1"}],
        },
    }

    updated_capacity = {"minimum": 1, "maximum": 900, "target": 1}
    ret = await hub.states.spotinst.ocean.aws.k8s_cluster.present(
        test_ctx,
        name=k8s_cluster_temp_name,
        resource_id=resource_id,
        controller_cluster_id=controller_cluster_id,
        region=region,
        compute=updated_compute,
        capacity=updated_capacity,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would update spotinst.ocean.aws.k8s_cluster '{resource_id}'" in ret["comment"]
    )

    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("old_state")
    assert_k8s_cluster_states(
        resource,
        compute,
        controller_cluster_id,
        k8s_cluster_temp_name,
        region,
        capacity,
    )
    resource = ret.get("new_state")
    assert_k8s_cluster_states(
        resource,
        updated_compute,
        controller_cluster_id,
        k8s_cluster_temp_name,
        region,
        updated_capacity,
    )

    # update k8s_cluster
    ret = await hub.states.spotinst.ocean.aws.k8s_cluster.present(
        ctx,
        name=k8s_cluster_temp_name,
        resource_id=resource_id,
        controller_cluster_id=controller_cluster_id,
        region=region,
        compute=updated_compute,
        capacity=updated_capacity,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated spotinst.ocean.aws.k8s_cluster '{resource_id}'" in ret["comment"]

    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("old_state")
    assert_k8s_cluster_states(
        resource,
        compute,
        controller_cluster_id,
        k8s_cluster_temp_name,
        region,
        capacity,
    )

    resource = ret.get("new_state")
    assert_k8s_cluster_states(
        resource,
        updated_compute,
        controller_cluster_id,
        k8s_cluster_temp_name,
        region,
        updated_capacity,
    )

    # Delete k8s_cluster with test flag
    ret = await hub.states.spotinst.ocean.aws.k8s_cluster.absent(
        test_ctx, name=k8s_cluster_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete spotinst.ocean.aws.k8s_cluster '{k8s_cluster_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert_k8s_cluster_states(
        resource,
        updated_compute,
        controller_cluster_id,
        k8s_cluster_temp_name,
        region,
        updated_capacity,
    )

    # Delete k8s_cluster
    ret = await hub.states.spotinst.ocean.aws.k8s_cluster.absent(
        ctx, name=k8s_cluster_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted spotinst.ocean.aws.k8s_cluster '{k8s_cluster_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert_k8s_cluster_states(
        resource,
        updated_compute,
        controller_cluster_id,
        k8s_cluster_temp_name,
        region,
        updated_capacity,
    )

    # Deleting k8s_cluster again should be an no-op
    ret = await hub.states.spotinst.ocean.aws.k8s_cluster.absent(
        ctx, name=k8s_cluster_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"spotinst.ocean.aws.k8s_cluster '{k8s_cluster_temp_name}' already absent"
        in ret["comment"]
    )


async def assert_k8s_cluster_states(
    resource, compute, controller_cluster_id, k8s_cluster_temp_name, region, capacity
):
    assert k8s_cluster_temp_name == resource.get("name")
    assert controller_cluster_id == resource.get("controller_cluster_id")
    assert region == resource.get("region")
    assert compute == resource.get("compute")
    assert capacity == resource.get("capacity")
