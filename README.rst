===============
idem-spotinst
===============

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/

.. note::
    Idem plugin to manage Spotinst resources

About
=====

Spotinst Provider Idem plugin

What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based
implementation of *Plugin Oriented Programming (POP)*. POP seeks to bring
together concepts and wisdom from the history of computing in new ways to solve
modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/saltstack/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/saltstack/pop/pop-create/>`__

Getting Started
===============

Prerequisites
-------------

* Python 3.7+
* git *(if installing from source, or contributing to the project)*

Installation
------------

.. note::

   If wanting to contribute to the project, and setup your local development
   environment, see the ``CONTRIBUTING.rst`` document in the source repository
   for this project.

If wanting to use ``idem-spotinst``, you can do so from source.

Install from source
+++++++++++++++++++

.. code-block:: bash

   # clone repo
   git clone git@<your-project-path>/idem-spotinst.git
   cd idem-spotinst

   # Setup venv
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -e .

Usage
=====

Credentials Setup
-----------------

After installation, the spotinst Idem execution and state modules will be accessible to the pop `hub`.
In order to use them, we need to set up our credentials.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop hub <https://pop-book.readthedocs.io/en/latest/main/hub.html#>`__

Create a new file called `credentials.yaml` and populate it with credentials.
The `default` profile will be picked up automatically by `idem`.

There are multiple authentication backends for `idem-spotinst` which each have their own unique set of parameters.
The following examples show the parameters that can be used to define credential profiles.

credentials.yaml:

..  code:: sls

    spotinst:
      default:
        account_id: act-11c833de
        token: b5460afe3c29a30c28abd54d190d1aa923587574321e75925cfc8268b54b4562

Next step is to encrypt the credentials file, and add the encryption key and encrypted file
path to the ENVIRONMENT.

Encrypt the credential file:

.. code:: bash

    Idem encrypt credentials.yaml

This will generate a credentials.yaml.fernet file and a command line output token::

    -AXFSEFSSEjsfdG_lb333kVhCVSCDyOFH4eABCDEFNwI=

Add these to your environment:

.. code:: bash

    export ACCT_KEY="-AXFSEFSSEjsfdG_lb333kVhCVSCDyOFH4eABCDEFNwI="
    export ACCT_FILE=$PWD/credentials.yaml.fernet


You are ready to use idem-spotinst!!!

STATES
--------
Idem states are used to make sure resources are in a desired state.
The desired state of a resource can be specified in sls file.
In Idem-spotinst, three states are supported: `present`, `absent`, `describe`

present state
+++++++++++++
`present` state makes sure a resource exists in a desired state. If a resource does
not exist, running `present` will create the resource on the provider. If a resource
exists, running `present` will update the resource on the provider. (Only the values
that the spotinst REST api supports can be updated.)

absent state
++++++++++++
`absent` state makes sure a resource does not exist. If a resource exits, running
`absent` will delete the resource. If a resource does not exist, running `absent`
is a no-operation.

describe state
++++++++++++++
`describe` state lists all the current resources of the same resource type
under the spotinst account id specified in the credential profile.

States can be accessed by their relative location in `idem-spotinst/idem_spotinst/states`.
For example, in the state sls yaml file below, spotinst launch specification state can be created with the `present` function.

my_resource_launch_spec_state.sls:

.. code:: sls

    my_resource_launch_spec:
        spotinst.ocean.aws.launch_spec.present:
          - name: my_resource_launch_spec
          - taints:
            - effect: NoSchedule
              key: taint1
              value: taint-value-1
          - tags:
            - tagKey: Env
              tagValue: test
          - subnet_ids:
            - subnet-06747be60363933d9
            - subnet-03b6d0dfc57e1gbd6
          - security_group_ids:
            - sg-08dcdf4874a2f5g04
            - sg-0844f76b69f16b8b8
          - ocean_id: o-b78b1e79
          - labels:
            - key: tag1
              value: value1
            - key: tag2
              value: value2
          - instance_types:
            - t2.micro
          - image_id: ami-0c02fb55956c7d318

The state sls file can be executed with:

.. code:: bash

    idem state $PWD/my_resource_launch_spec_state.sls

The resource parameters in an sls yaml file follow the exact structure as
what's in the `Spotinst REST api doc <https://docs.spot.io/api/>`__ . URI Parameters
should be specified in snake case with "- " in front. All parameters of the api request body
should be specified in exactly the same way as what's in the spotinst REST api.

Current Supported Resources states
++++++++++++++++++++++++++++++++++

ocean_aws
"""""""""""""
k8s_cluser

launch_spec
